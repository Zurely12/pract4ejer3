package ito.poo.clases;

import java.util.HashSet;

public class CuerpoCeleste {
	
	private String nombre=  " ";
	private HashSet<Ubicacion> localizacion;
	private String composicion=  " ";
	
	public CuerpoCeleste(String nombre, HashSet<Ubicacion> localizacion, String composicion) {
		super();
		this.nombre = nombre;
		this.localizacion = localizacion;
		this.composicion = composicion;
	}
public float desplazamiento(int i, int j  ) {
		float  desplazamiento=i;
		return this.desplazamiento(i);
		
	}

public float desplazamiento(int j ) {
	float  desplazamiento=j;
	return desplazamiento;
	
}


	public HashSet<Ubicacion> getLocalizacion() {
		return this.localizacion;
}



	public void setLocalizacion(HashSet<Ubicacion> localizacion) {
		this.localizacion = localizacion;
	}



	public String getComposicion() {
		return composicion;
	}



	public void setComposicion(String composicion) {
		this.composicion = composicion;
	}



	public String getNombre() {
		return nombre;
	}
	public String toString() {
		return "CuerpoCeleste [nombre=" + nombre + ", localizacion=" + localizacion + ", composicion=" + composicion
				+ "]";
	}
}
