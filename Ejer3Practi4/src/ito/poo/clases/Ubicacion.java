package ito.poo.clases;

public class Ubicacion {
	
	private float longitud= 0F;;
	private float latitud= 0F;;
	private String periodo=  " ";
	private float distancia= 0F;;

public Ubicacion(float longitud, float latitud, String periodo, float distancia) {
		super();
		this.longitud = longitud;
		this.latitud = latitud;
		this.periodo = periodo;
		this.distancia = distancia;
	}

public float getDistancia() {
	return distancia;
}


public void setDistancia(float distancia) {
	this.distancia = distancia;
}


public float getLongitud() {
	return longitud;
}


public float getLatitud() {
	return latitud;
}


public String getPeriodo() {
	return periodo;
}

public String toString() {
	return "Ubicacion [longitud=" + longitud + ", latitud=" + latitud + ", periodo=" + periodo + ", distancia="
			+ distancia + "]";
}

}
